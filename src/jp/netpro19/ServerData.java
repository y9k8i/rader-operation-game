package jp.netpro19;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServerData {

	private StringProperty serverName;
    private StringProperty description;
    private StringProperty address;
    private StringProperty pingTime;

    public ServerData(String serverName, String description, String address, String pingTime) {
        this.serverName = new SimpleStringProperty(serverName);
        this.description = new SimpleStringProperty(description);
        this.address = new SimpleStringProperty(address);
        this.pingTime  = new SimpleStringProperty(pingTime);
    }

    public StringProperty serverNameProperty() {
        return serverName;
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public StringProperty addressProperty() {
        return address;
    }

    public StringProperty pingTimeProperty() {
        return pingTime;
    }

}
