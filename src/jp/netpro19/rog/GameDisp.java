package jp.netpro19.rog;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;

public class GameDisp extends Application{
		public void start(Stage primaryStage) {
			try {
				//FXMLの指定
				SplitPane root = (SplitPane)FXMLLoader.load(getClass().getResource("GameMy_Screen3.fxml"));
				//SplitPane SceneSeverSelect = (SplitPane)FXMLLoader.load(getClass().getResource("IpSelectScreen.fxml"));
				//ウィンドウタイトルをつける
				primaryStage.setTitle("レーダー作戦ゲーム");
				//ウィンドウの大きさを指定
				Scene scene = new Scene(root);
				//Scene scene2 = new Scene(SceneSeverSelect);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}

		public static void main(String[] args) {
			launch(args);
		}

}
