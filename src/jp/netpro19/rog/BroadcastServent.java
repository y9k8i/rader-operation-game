package jp.netpro19.rog;

import static jp.netpro19.rog.Main.DEBUG;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ネットワークに関わることは全部このクラスが担当する。<br>
 * ２回以上インスタンス化してもいいことになった。<br>
 * マルチキャスト通信は<a href=
 * "https://nompor.com/2018/12/04/post-3742/">このページ</a>を参考にした<br>
 * シリアライズ・デシリアライズは<a href=
 * "http://tsukaayapontan.web.fc2.com/doc/serialize/serialize.html">このページ</a>を参考にした<br>
 * 整数とバイト配列の相互変換は<a href=
 * "https://qiita.com/nwtgck/items/c53db76e19ef80296a71">このページ</a>を参考にした
 */
public class BroadcastServent {

	/**
	 * ROGをUnicodeでエンコードしたU+52U+4FU+47から。<br>
	 * 49152から65535は自由に使える。
	 */
	private static final int PORT = 52447;
	String defaultAddress = "192.168.0.255"; //初期設定用
	public static DatagramSocket socket;
	private ByteArrayOutputStream bos = null;
	private ObjectOutputStream oos = null;
	private ByteArrayInputStream bis = null;
	private ObjectInputStream ois = null;
	private DatagramPacket packet = null;
	private InetSocketAddress address;
	/** 送信元IPアドレス */
	private String hostaddress = null;
	/** 時間をミリ秒で格納 */
	private static long timems;
	private static int pingms;

	// データ
	byte[] data = null;
	byte[] lengthbuf = new byte[4];
	static RogData pongdata = null;
	int length;
	private static boolean initFlag = false;
	private static boolean receiving = false;
	/** 設定中フラグ この間は受信を行わない */
	static boolean setting = false;
	/** 重複チェック用 画面に追加する処理は別に必要 */
	static ArrayList<ServerInfo> servers = new ArrayList<ServerInfo>();

	/** すべてのサーバの情報を取得する */
	public void queryServer() throws SocketException {
		Main.sendRogData(new RogData("query", null));
	}

	public BroadcastServent() {
		if (DEBUG)
			System.out.println(this.getClass().getName() + "インスタンス化");
		Pattern p = Pattern.compile("([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})");
		Matcher m = p.matcher(getMyHostAddress());
		if (m.find()) {
			defaultAddress = m.group(1) + "." + m.group(2) + "." + m.group(3) + ".255";
		}
		if (initFlag == false) {
			init();
			initFlag = true;
		}
		System.out.println("設定アドレス：" + defaultAddress);
	}

	/**
	 * 初期化をするメソッド<br>ソケットを作る時にのみ呼び出す
	 */
	private void init() {
		try {
			address = new InetSocketAddress(defaultAddress, PORT);
			socket = new DatagramSocket(PORT);
		} catch (IOException e) {
			System.err.println(this.getClass().getName() + "のネットワーク初期化に失敗しました");
			e.printStackTrace();
		}
		try {
			bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
		} catch (IOException e) {
			System.err.println(this.getClass().getName() + "のストリーム初期化に失敗しました");
			e.printStackTrace();
		}
	}

	/**
	 * データを受信するメソッド。一つ受信したら終了する。
	 */
	public RogData recvData() {
		if (receiving) {
			System.err.println("エラー: 受信メソッド多重起動\n\tat " + ThreadUtils.calledFrom());
			return null;
		} else {
			receiving = true;
			RogData obj = null;
			ois = null;
			try {
				// フェーズ1 これから来るデータのバイト数をintで受け取る
				packet = new DatagramPacket(lengthbuf, 4);
				socket.receive(packet);
				length = ByteBuffer.wrap(lengthbuf).getInt();
				hostaddress = packet.getAddress().getHostAddress().toString();
				if (DEBUG)
					System.out.println(hostaddress + "から" + length + "バイトのデータを受信します");
				// フェーズ2 データを受信する
				data = new byte[length];
				packet = new DatagramPacket(data, length);
				socket.receive(packet);
				hostaddress = packet.getAddress().getHostAddress();
				bis = new ByteArrayInputStream(data);
				ois = new ObjectInputStream(bis);
				obj = (RogData) ois.readObject();
				if (DEBUG)
					System.out.println(hostaddress + "からデータを受信します");
					System.out.println("受信するデータは: " + obj);
			} catch (SocketTimeoutException e) {
				if (DEBUG)
					System.out.println("受信待機ソケットがタイムアウトしました");
			} catch (SocketException e) {
				if (DEBUG)
					System.out.println("受信待機ソケットが閉じました");
			} catch (IOException | ClassNotFoundException e) {
				System.err.println(this.getClass().getName() + "内でエラーが発生しました");
				e.printStackTrace();
			} finally {
				receiving = false;
				try {
					if (bis != null)
						bis.close();
					if (ois != null)
						ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return obj;
		}
	}

	/**
	 * データを送信するメソッド。スレッドとして起動させる。
	 */
	public void sendData(RogData obj) {
		try {
			bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			// フェーズ1 これから送信するデータをバイト型配列にする
			oos.writeObject(obj);
			data = bos.toByteArray();
			// フェーズ2 これから送信するデータのバイト数をintで送る
			length = data.length;
			if (DEBUG)
				System.out.println(length + "バイトのデータを送信します\n\tat " + ThreadUtils.calledFrom());
			lengthbuf = ByteBuffer.allocate(4).putInt(length).array();
			packet = new DatagramPacket(lengthbuf, 4, address);
			socket.send(packet);
			// フェーズ3 データを送信する
			if (DEBUG)
				System.out.println("送信するデータは: " + obj);
			packet = new DatagramPacket(data, length, address);
			socket.send(packet);
		} catch (IOException e) {
			System.err.println(this.getClass().getName() + "内でエラーが発生しました");
			e.printStackTrace();
		} finally {
			try {
				if (bos != null)
					bos.close();
				if (oos != null)
					oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * タイムアウト時間を設定する 受信時のみ有効
	 *
	 * @param ms ミリ秒 0で無効になる
	 */
	public static void setTimeout(int ms) {
		if (DEBUG)
			System.out.println("受信タイムアウトが" + ms + "msに設定されます\n\tat " + ThreadUtils.calledFrom());
		if (DEBUG && !BroadcastServent.setting)
			System.err.println("settingが偽のため反映されません！");
		try {
			socket.setSoTimeout(ms);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public boolean ping(String address) {
		boolean result = false;
		pingms = -1;
		if (DEBUG)
			System.out.println(address + "へのpingスタート \n\tat " + ThreadUtils.calledFrom());
		pongdata = null;
		// ここから順番大事
		setting = true;
		socket.close();
		init();
		setTimeout(500);
		timems = System.currentTimeMillis();
		Main.sendRogData(new RogData("ping", address));
		setting = false;
		while (System.currentTimeMillis() - timems < 500) {
			if (pongdata == null)
				continue;
			else if (pongdata.getType() == "pong" && pongdata.getData() == address) {
				pingms = (int) (System.currentTimeMillis() - timems);
				result = true;
				break;
			} else
				System.out.println("pong以外のデータを受信しました 継続してpongの受信を試みます");
		}
		// ここから順番大事
		setting = true;
		socket.close();
		init();
		setTimeout(0);
		setting = false;
		return result;
	}

	/** 直前に受信したデータのIPアドレスを取得 */
	public String getHostAddress() {
		return hostaddress;
	}

	/** 自分のIPアドレスを取得 */
	public String getMyHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			System.err.println("自分のIPアドレスを取得できません\n\tat " + ThreadUtils.calledAt());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 直前に実行したpingのミリ秒を取得<br>
	 * ping失敗時は-1が返る
	 */
	public static int getPingms() {
		return pingms;
	}

	public void interrupt() {
			if (DEBUG)
				System.out.println(this.getClass().getName() + "の割り込み処理開始");
			socket.close();
	}

}
