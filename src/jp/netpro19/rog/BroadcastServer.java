package jp.netpro19.rog;

import static jp.netpro19.rog.ConfigManager.*;

import javafx.application.Platform;

/**
 * UDPマルチキャスト通信をずっと待ち受け続けるクラス。スレッドとして起動させる。<br>
 *  一つのインスタンスで一つだけ起動させる。NetworkUDPControlより後に。
 *
 * @param int port
 */
public class BroadcastServer extends Thread {
	private static BroadcastServent servent = Main.servent;
	private RogData data = null;
	private static ServerInfo sInfo = new ServerInfo(prop.getProperty("servername"),
			prop.getProperty("motd"));

	@Override
	public void run() {
		System.out.println("UDPパケット受信スレッド起動");
		do {
			try {
				data = servent.recvData();
				if (this.isInterrupted())
					break;
				switch (data.getType()) {
				case "query":
					if (!servent.getHostAddress().equals(servent.getMyHostAddress()) && !Main.getStartGameFlag()) {
						System.out.println("getHostAddress=" + servent.getHostAddress() + "getMyHostAddress="
								+ servent.getMyHostAddress());
						try {
							Thread.sleep((long) (Math.random() * 50));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						Main.sendRogData(new RogData("sInfo", sInfo));
					} else
						System.out.println("自分からのクエリのため応答しません");
					break;
				case "sInfo":
					ServerInfo sInfo = (ServerInfo) data.getData();
					sInfo.setAddress(servent.getHostAddress());
					if (IpSelectScreenController.isVisible) {
						if (!BroadcastServent.servers.contains(sInfo)
								&& !servent.getHostAddress().equals(servent.getMyHostAddress())) {
							BroadcastServent.servers.add(sInfo);
							Main.loader.<IpSelectScreenController> getController().addList(sInfo);
						}
					} else
						System.out.println("サーバ情報を受信しましたがクエリ中でないため何もしません");
					break;
				case "ping":
					if (data.getData().equals(servent.getMyHostAddress())) {
						if (Main.getHostFlag() && Main.data.getSetupFlag()) {
							Main.sendRogData(new RogData("pong", data.getData()));
						} else if (!Main.getHostFlag() && !Main.getStartGameFlag()) {
							if (!servent.getHostAddress().equals("192.168.1.255")
									&& !servent.getHostAddress().equals("127.0.0.1")) {
								Main.address = servent.getHostAddress();
								Main.sendRogData(new RogData("pong", data.getData()));
								Main.nonUIControlPop(1, "ホストとして設定します", "GameMy_Screen.fxml");
								Main.setHosrFlag(true);
								Main.data.setSetupFlag(true);
							}
						}
					} else if (data.getData().equals("test")) {
						Main.address = servent.getHostAddress();
						RogData rog = new RogData("pong", "");
						Main.sendRogData(rog);
					} else {
						System.out.println("pingが来たよ");
					}
					break;
				case "pong":
					if (!servent.getHostAddress().equals(servent.getMyHostAddress())
							&& servent.getHostAddress().equals(Main.address) && data.getData().equals(Main.address)) {
						Main.nonUIControlPop(3, "ホスト側の応答を待ちます", "");
						Main.setStartGameFlag(true);
						Main.data.setSetupFlag(true);
						Main.data.setTurn(true); //一時処理用
					} else {
						System.out.println("自分のは破棄");
					}
					break;
				case "chat":
					// チャット画面にテキストを追加
					if (Main.data.getSetupFlag()) {
						System.out.println("セットアップ中はチャットができません");
					} else {
						System.out.println("Server: チャット受信しました");
						if (!servent.getHostAddress().equals(servent.getMyHostAddress())) {
							Main.setChatFlag(true);

							//https://qiita.com/Shiratori/items/7febeb43bdcbcdba6508参考
							new Thread(() -> Platform
									.runLater(() -> Main.loader.<GameMyScreenMainConroller> getController()
											.sendMessage((String) data.getData()))).start();
						}

					}

					break;
				case "shot"://今は二人用にカスタマイズ
					System.out.println("攻撃の判定");
					if (!servent.getHostAddress().equals(servent.getMyHostAddress())
							&& servent.getHostAddress().equals(Main.address)) {
						// 当たったか水しぶきかを返信
						InputFromatter tMap = new InputFromatter((String) data.getData());
						System.out.println("攻撃受信" + tMap.getText());
						Main.sendRogData(new RogData("chat",
								"Log：" + servent.getMyHostAddress() + "の" + tMap.getText() + "に攻撃"));

						if (Main.hitCheck(tMap.convString())) {
							Main.sendRogData(new RogData("chat", "Log：戦艦にヒット"));
							if (!Main.data.checkMyShipFlag()) {
								Main.sendRogData(new RogData("quit", "Log：あなたの勝ち"));
								Main.endFlag = true;
								Main.nonUIControlPop(1, "あなたの負けです", "end");
							}
						} else {
							Main.sendRogData(new RogData("chat", "Log：水しぶきだったようだ"));
						}
						Main.data.setTurn(true);
						if (!Main.endFlag) {
							Main.nonUIControlPop(1, "自分のターンになります", "GameMy_ScreenMain.fxml");
							String tmpString = servent.getMyHostAddress();
							new Thread(() -> servent.sendData(new RogData("chat", "Log：" + tmpString + "のターンです")));
							Main.sendRogData(new RogData("change", Main.address));
							Platform.runLater(() -> Main.loader.<GameMyScreenMainConroller> getController()
									.setChatBox());
						}
					} else {
						System.out.println("対象外です");
					}
					break;
				case "change"://今は二人用にカスタマイズ
					if (!servent.getHostAddress().equals(servent.getMyHostAddress())
							&& servent.getHostAddress().equals(Main.address)
							&& servent.getMyHostAddress().equals((String) data.getData())) {
						if (!Main.data.getTurn()) {
							System.out.println("Opponent Turn");
						} else if (Main.data.getTurn() && !Main.endFlag) {
							Main.data.setTurn(false);
							Main.nonUIControlPop(1, "相手のターンになります", "GameMy_ScreenMain.fxml");
							Platform.runLater(() -> Main.loader.<GameMyScreenMainConroller> getController()
									.setChatBox());
						}
					}

					break;
				case "map":
					if (!servent.getHostAddress().equals(servent.getMyHostAddress())
							&& servent.getHostAddress().equals(Main.address)) {
						if (Main.getStartGameFlag() && Main.data.getSetupFlag()) {
							Main.setGameData(1, (int[][]) data.getData());
							System.out.println("地図取得");
							Main.nonUIControlPop(0, "", "");
							Main.nonUIControlPop(1, "ゲスト側の設定です", "GameMy_Screen.fxml");
						} else if (Main.getHostFlag() && Main.data.getSetupFlag()) {
							Main.setGameData(1, (int[][]) data.getData());
							Main.data.setSetupFlag(false);
							Main.setStartGameFlag(true);
							System.out.println("地図取得");
							Main.nonUIControlPop(0, "", "");
							Main.nonUIControlPop(4, "application.css", "GameMy_ScreenMain.fxml");
						}
					}
					break;
				case "quit":
					// ゲーム終了
					if (!servent.getHostAddress().equals(servent.getMyHostAddress())
							&& servent.getHostAddress().equals(Main.address) && Main.getStartGameFlag()) {
						System.out.println("終了");
						if (data.getData().equals("Log：あなたの勝ち")) {
							Main.sendRogData(new RogData("chat", "Log：あなたの負け"));
							Main.nonUIControlPop(1, "あなたの勝ちです", "end");
						} else {
							Main.nonUIControlPop(1, "あなたの負けです", "end");
						}
					}
					break;
				default:
					System.out.println("未知のデータを受信: " + data);
				}
			} catch (NullPointerException e) {
				System.err.println("ぬるぽ at " + ThreadUtils.calledAt());
				e.getStackTrace();
				new Thread(Main::startServer).start();
			}
		} while (!this.isInterrupted());
		System.out.println("UDPパケット受信スレッド終了");
	}

	@Override
	public void interrupt() {
		System.out.println(this.getClass().getName() + "の割り込み処理開始");
		servent.interrupt();
		super.interrupt();
	}

}
