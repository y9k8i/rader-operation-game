package jp.netpro19.rog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class GameMyScreenConroller implements Initializable {
	private RogData rogData;
	private final int SPACE = 5;
	private final int battleShip = 5; //1マス用
	private int longBattleShip = 3; //3マス用
	private int veryLongBattleShip = 2; //5マス用
	private final int X = 10;
	private final int Y = 10;
	private int[][] shipFlag = new int[X][Y]; //船があるか管理するフラグ
	private int Counter = 0;
	private String text = "";
	private int setupNum = 0;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private GridPane GameField;

	@FXML
	private Label statusLabel;

	@FXML
	private Label explainLabel;

	@FXML
	private ScrollPane scroll;

	@FXML
	private VBox message_display;

	@FXML
	private TextField ChatField;

	@FXML
	private Button sendButton;

	@FXML
	private Button btn[][];

	@FXML
	void sendAction(ActionEvent event) {
		//呼び出されたコントローラ
		Object source = event.getSource();
		//ボタンにキャストできるなら
		if (source instanceof Button) {
			//ボタンのIDを取得して引数にする
			sendMessage(((Button) source).getId());
		}

	}

	public void sendMessage(String message) { //サンプル参考
		text = message;
		sendChatPane(ChatField);
	}

	private void sendChatPane(TextField message) { //サンプルから引っ張ってきたもの

		Label label = new Label();

		//チャットテキストが空でないなら
		if (!message.getText().equals("")) {

			//チャットテキストを表示させるLabelを作る
			label = new Label(message.getText());

			//ネットワークで送信するにはここらへんに追記必須
			if (!Main.data.getSetupFlag()) {
				rogData = new RogData("chat", message.getText());
				Main.sendRogData(rogData);
			}

			//LabelにIDを設定して、Application2.css内のcssを適用させる
			label.setId("chat");
			//チャット入力欄の文字を消す
			message.setText("");

		} else if (Main.getChatFlag()) {

			//チャットテキストを表示させるLabelを作る
			label = new Label(text);

			//AnchorPaneの左上の座標から見て、SPACE分だけ下にLabelをずらす
			label.setLayoutY(SPACE);
			//LabelにIDを設定して、Application2.css内のcssを適用させる
			label.setId("chat");

			System.out.println("受信しました");
		}

		//BorderPaneをVBoxに追加
		message_display.getChildren().add(label);

		//スクロールを後方に移動
		scroll.setVvalue(1.);
	}

	@Override
	//パネルのボタン初期化（https://yamakasa3.hatenablog.com/entry/2018/07/16/120000一部参照）
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ
		btn = new Button[X][Y]; // (列,行)
		setupNum = Main.data.getSetupNum();
		if (setupNum > 0) {
			shipFlag = Main.getGameData(0); //船があるか管理するフラグ
			message_display.getChildren().addAll(Main.data.getChatBox());
			if(setupNum == 1) explainLabel.setText("　次に3マス幅の戦艦を3か所選択してください (左右1マスずつ空くように選ぶといい)");
			else if(setupNum == 2) explainLabel.setText("次に5マス幅の戦艦を2か所選択してください（横、縦の順に1種類ずつ）");
			else explainLabel.setText("エラーが発生しました。もう一度やり直してください。");
		}
		for (int i = 0; i < btn.length; i++) {
			for (int j = 0; j < btn[i].length; j++) {
				AnchorPane panel = new AnchorPane();
				btn[i][j] = new Button();
				btn[i][j].setPrefWidth(10);
				btn[i][j].setPrefHeight(10);
				btn[i][j].setAlignment(Pos.CENTER);
				btn[i][j].setMinHeight(0.0);
				btn[i][j].setMinWidth(0.0);
				String num = String.valueOf(i + 1) + "," + String.valueOf(j + 1); //id設定用
				btn[i][j].setId("selectPane" + num);
				btn[i][j].setOnAction(event -> onClickPane(event));
				//色分け
				if (shipFlag[i][j] == 1)
					btn[i][j].setStyle("-fx-base: red");
				else if (shipFlag[i][j] == 2)
					btn[i][j].setStyle("-fx-base: green");
				else if (shipFlag[i][j] == 3)
					btn[i][j].setStyle("-fx-base: blue");

				panel.setPrefSize(200.0, 200.0);
				AnchorPane.setBottomAnchor(btn[i][j], 2.0);
				AnchorPane.setTopAnchor(btn[i][j], 1.0);
				AnchorPane.setLeftAnchor(btn[i][j], 1.0);
				AnchorPane.setRightAnchor(btn[i][j], 2.0);
				panel.getChildren().add(btn[i][j]);
				GridPane.setConstraints(panel, i, j);
				GameField.getChildren().add(panel);

			}
		}
	}

	public void onClickPane(ActionEvent event) { //パネルクリック用
		InputFromatter idCoord = new InputFromatter(((Button) event.getSource()).getId()); //idから座標の抽出用
		int num[] = idCoord.convString();
		System.out.println("クリックしたのは、" + num[0] + "," + num[1] + "デスネ");
		int x = num[0] - 1;
		int y = num[1] - 1;

		switch (setupNum) {
		case 0:
			if (Counter < battleShip) {
				if (btn[x][y].getStyle().equals("")) {
					Counter++;
					btn[x][y].setStyle("-fx-base: red");
					shipFlag[x][y] = 1;
				} else {
					if (Counter > 0) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
					}
				}
				if (Counter == battleShip) {
					new Main().popViewSelect("これでよろしいですか？", "GameMy_Screen.fxml", 1);
					Main.setGameData(0, shipFlag);
					Main.setChatBox(message_display);
				}
			} else {
				if (btn[x][y].getStyle().equals("")) {
					new Main().popViewSelect("これでよろしいですか？", "GameMy_Screen.fxml", 1);
					Main.setGameData(0, shipFlag);
					Main.setChatBox(message_display);
				} else {
					if (Counter > 0) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
					}
				}
			}
			break;
		case 1:
			if (Counter < longBattleShip) {
				if (btn[x][y].getStyle().equals("")) {
					if ((x > 0 && x < X - 1 && shipFlag[x - 1][y] > 0 && shipFlag[x + 1][y] > 0)
							&& (y > 0 && y < Y - 1 && shipFlag[x][y - 1] > 0 && shipFlag[x][y + 1] > 0)) { //四方囲み
						new Main().popView("選択できません", "");
					} else if (x == 0 && y == 0 && (shipFlag[x + 1][y] > 0 || shipFlag[x + 2][y] > 0
							|| shipFlag[x][y + 1] > 0 || shipFlag[x][y + 2] > 0)) { //四つ角判定左上
						new Main().popView("選択できません", "");
					} else if (x == (X - 1) && y == (Y - 1) && (shipFlag[x - 1][y] > 0 || shipFlag[x - 2][y] > 0
							|| shipFlag[x][y - 1] > 0 || shipFlag[x][y - 2] > 0)) { //四つ角判定右下
						new Main().popView("選択できません", "");
					} else if (x == (X - 1) && y == 0 && (shipFlag[x - 1][y] > 0 || shipFlag[x - 2][y] > 0
							|| shipFlag[x][y + 1] > 0 || shipFlag[x][y + 2] > 0)) { //四つ角判定右上
						new Main().popView("選択できません", "");
					} else if (x == 0 && y == (Y - 1) && (shipFlag[x + 1][y] > 0 || shipFlag[x + 2][y] > 0
							|| shipFlag[x][y - 1] > 0 || shipFlag[x][y - 2] > 0)) { //四つ角判定左下
						new Main().popView("選択できません", "");
					} else {
						Counter++;
						btn[x][y].setStyle("-fx-base: green");
						shipFlag[x][y] = 2;
						if (Counter == 1)
							new Main().popView("向きはランダムです", ""); //向き設定は今回省略しています
					}

				} else if (btn[x][y].getStyle().equals("-fx-base: red")) {
					new Main().popView("既に選択済みです", "");
				} else {
					if (Counter > 0) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
					}
				}
				if (Counter == longBattleShip) {
					new Main().popViewSelect("これでよろしいですか？", "GameMy_Screen.fxml", 2);
					Main.setChatBox(message_display);
				}
			} else {
				if (btn[x][y].getStyle().equals("")) {
					new Main().popViewSelect("これでよろしいですか？", "GameMy_Screen.fxml", 2);
					Main.setChatBox(message_display);
				} else {
					if (Counter > 0) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
					}
				}
			}
			break;

		case 2:
			if (Counter < veryLongBattleShip) {
				if (Counter == 0 && btn[x][y].getStyle().equals("") ) {
					if (x > 1 && x < (X - 2) && btn[x + 1][y].getStyle().equals("") && btn[x - 1][y].getStyle().equals("")
							&& btn[x + 2][y].getStyle().equals("") && btn[x - 2][y].getStyle().equals("")) {
						Counter++;
						btn[x][y].setStyle("-fx-base: blue");
						shipFlag[x][y] = 3;
						shipFlag[x + 1][y] = 3;
						shipFlag[x + 2][y] = 3;
						shipFlag[x - 1][y] = 3;
						shipFlag[x - 2][y] = 3;
					} else {
						new Main().popView("左右2マスずつあいているか確認してください", "");
					}
				}else if (Counter == 1 && btn[x][y].getStyle().equals("")) {
					if (y > 1 && y < (Y - 2) && btn[x][y + 1].getStyle().equals("") && btn[x][y - 1].getStyle().equals("")
							&& btn[x][y + 2].getStyle().equals("") && btn[x][y - 2].getStyle().equals("")) {
						Counter++;
						btn[x][y].setStyle("-fx-base: blue");
						shipFlag[x][y] = 3;
						shipFlag[x][y + 1] = 3;
						shipFlag[x][y + 2] = 3;
						shipFlag[x][y - 1] = 3;
						shipFlag[x][y - 2] = 3;
					} else {
						new Main().popView("上下2マスずつあいているか確認してください", "");
					}
				} else if (btn[x][y].getStyle().equals("-fx-base: red") || btn[x][y].getStyle().equals("-fx-base: green")) {
					new Main().popView("既に選択済みです", "");
				} else {
					if (Counter == 2) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
						shipFlag[x][y + 1] = 0;
						shipFlag[x][y + 2] = 0;
						shipFlag[x][y - 1] = 0;
						shipFlag[x][y - 2] = 0;
					} else if (Counter == 1) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
						shipFlag[x + 1][y] = 0;
						shipFlag[x + 2][y] = 0;
						shipFlag[x - 1][y] = 0;
						shipFlag[x - 2][y] = 0;
					}
				}
				if (Counter == veryLongBattleShip) {
					new Main().popViewSelect("これでよろしいですか？", "load", 0);
					Main.setChatBox(message_display);
				}
			} else {
				if (btn[x][y].getStyle().equals("")) {
					new Main().popViewSelect("これでよろしいですか？", "load", 0);
					Main.setChatBox(message_display);
				} else {
					if (Counter == 2) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
						shipFlag[x][y + 1] = 0;
						shipFlag[x][y + 2] = 0;
						shipFlag[x][y - 1] = 0;
						shipFlag[x][y - 2] = 0;
					} else if (Counter == 1) {
						Counter--;
						btn[x][y].setStyle("");
						shipFlag[x][y] = 0;
						shipFlag[x + 1][y] = 0;
						shipFlag[x + 2][y] = 0;
						shipFlag[x - 1][y] = 0;
						shipFlag[x - 2][y] = 0;
					}
				}
			}
			break;
		}
	}

	@FXML
	void initialize() {
		assert GameField != null : "fx:id=\"GameField\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";
		assert statusLabel != null : "fx:id=\"statusLabel\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";
		assert explainLabel != null : "fx:id=\"explainLabel\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";
		assert scroll != null : "fx:id=\"scroll\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";
		assert message_display != null : "fx:id=\"message_display\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";
		assert ChatField != null : "fx:id=\"ChatField\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";
		assert sendButton != null : "fx:id=\"sendButton\" was not injected: check your FXML file 'GameMy_Screen.fxml'.";

	}

}
