package jp.netpro19.rog;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputFromatter { // 入力の判別プログラム（作りかけ）
	private String text;

	public InputFromatter(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public int[] convString() { // 数値と文字列の変換処理
		Pattern p = Pattern.compile("[.+]{0,}([0-9]{1,2}),([0-9]{1,2})"); //マッチさせたいパターン作成
		Matcher m = p.matcher(text);
		int num[] = { 0, 0 };
		if (m.find()) {
			num[0] = Integer.parseInt(m.group(1));
			num[1] = Integer.parseInt(m.group(2));
		}
		return num;
	}

	public boolean checkNumOnly(String t) { //テキストからの数字の入力判別
		int checkNumTmp;
		try {
			checkNumTmp = Integer.parseInt(t);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public boolean checkIpFormat() { //(***.***.***.***と0～255かを判別
		if (text.matches("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}")) { //数字なのかを簡単に判別（エラー防止）
			Pattern p = Pattern.compile("([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})"); //マッチさせたいパターン作成
			Matcher m = p.matcher(text);
			if (m.find()) {
				int[] ipNum = { 0, 0, 0, 0 };
				checkNumOnly(m.group(1));
				ipNum[0] = Integer.parseInt(m.group(1));
				ipNum[1] = Integer.parseInt(m.group(2));
				ipNum[2] = Integer.parseInt(m.group(3));
				ipNum[3] = Integer.parseInt(m.group(4));

				for (int i = 0; i < ipNum.length; i++) {
					if (ipNum[i] > 255 || ipNum[i] < 0)
						return false;
				}
				System.out.println("Probably, IP address");
			return true;
			}
			return false;
		} else {
			return false;
		}
	}
}
