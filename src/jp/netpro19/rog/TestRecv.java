package jp.netpro19.rog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

/**
 * 受信テスト用クラス ブロードキャストにも対応
 */
public class TestRecv {

	public static void main(String[] args) {
		/** ブロードキャスト */
		BroadcastServent broadcastServent = null;
		/** マルチキャスト */
		MulticastServent multicastServent = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		boolean useBroadcast = true;

		try {
			System.out.print("マルチキャストかブロードキャストかを入力[m(ulticast), b(roadcast), quit]: ");
			line = reader.readLine();
			switch (line) {
			case "m":
			case "multicast":
				System.out.println("マルチキャストに設定されました");
				useBroadcast = false;
				break;
			case "b":
			case "broadcast":
				System.out.println("ブロードキャストに設定されました");
				useBroadcast = true;
				break;
			default:
				System.err.println("判別不可: " + line);
				System.err.println("ブロードキャストに設定します");
			}

			if (useBroadcast)
				broadcastServent = new BroadcastServent();
			else
				multicastServent = new MulticastServent();

			if (useBroadcast) {
				System.out.println("接続先のアドレスは" + BroadcastServent.socket.getInetAddress());
				while (true)
					broadcastServent.recvData();
			} else {
				System.out.println("ネットワークインタフェースのIPアドレスは" + MulticastServent.socket.getInterface().getHostAddress());
				System.out.println("マルチキャストネットワークインタフェースセットの名前は" + MulticastServent.socket.getNetworkInterface().getName());
				System.out.println("このクライアントのIPアドレスは" + InetAddress.getLocalHost().getHostAddress());
				while (true)
					multicastServent.recvData();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
