package jp.netpro19.rog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class PopLoadScreenController implements Initializable{

	static boolean isVisible = true;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox messageDisp;

    @FXML
    private Label messageLable;


    @Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ
		String message = Main.getText();
		messageLable.setText(message);
	}

    @FXML
    void initialize() {
        assert messageDisp != null : "fx:id=\"messageDisp\" was not injected: check your FXML file 'PopLoadScreen.fxml'.";
        assert messageLable != null : "fx:id=\"messageLable\" was not injected: check your FXML file 'PopLoadScreen.fxml'.";

    }
}

