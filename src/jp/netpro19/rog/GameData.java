package jp.netpro19.rog;

import java.util.Random;

import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

public class GameData {
	private int[][] playerData;
	private int[][] opponentData; //サーバーからその都度受信するため1つのみ
	private static VBox chatBox;
	private boolean myTurn = false; //いじってとりあえず設定
	private boolean setupFlag = false;
	private int setupNum = 0;

	public GameData(int[][] data) {
		setPlayerData(data);
	}

	public void setPlayerData(int[][] data) {
		playerData = data;
	}

	public void setOpponentData(int[][] data) {
		opponentData = data;
	}

	public void setSetupFlag(boolean flag) {
		setupFlag = flag;
	}

	public void setSetupNum(int num) {
		setupNum = num;
	}

	/**
	 * チャットの画面遷移時に消えないよう保持するセッター
	 * @param box	チャットがあるVbox
	 */
	public void setChatData(VBox box) {
		if (Main.DEBUG)
			System.out.println("setChatData(" + box + ")");
		chatBox = new VBox();
		chatBox.setAlignment(Pos.TOP_LEFT);
		chatBox.setId("message_display");
		chatBox.setPrefHeight(0.0);
		chatBox.setPrefWidth(366.0);
		chatBox.setFillWidth(true);
		chatBox.getChildren().addAll(box);
	}

	public void sendOpponetData(int[][] data) {
		opponentData = data;

		//ネットワークで送信
		RogData sendRog = new RogData("map", opponentData);
		Main.sendRogData(sendRog);
	}

	public void setTurn(boolean flag) {
		myTurn = flag;
	}

	public void setOneData(int x, int y, int data) {
		playerData[x][y] = data;
	}

	public int[][] getData() {
		return playerData;
	}

	public boolean getTurn() {
		return myTurn;
	}

	public boolean getSetupFlag() {
		return setupFlag;
	}

	public int getSetupNum() {
		return setupNum;
	}

	public int[][] getOpponentData() {
		return opponentData;
	}

	public VBox getChatBox() { //画面遷移対策
		return chatBox;
	}

	public int getOneData(int x, int y, int mode) {
		if (mode == 1) {
			return opponentData[x][y];
		} else {
			return playerData[x][y]; //null回避用
		}
	}

	//自分のマップについては作成後、送信（アイテムは送信後にサーバー側で設置(2つほど)し、マップデータに加えて保管）
	public void sendData() {
		int sendData[][] = new int[playerData.length][playerData[0].length];
		for (int i = 0; i < playerData.length; i++) {
			for (int j = 0; j < playerData[i].length; j++) {
				if (playerData[i][j] > 0 && playerData[i][j] < 4) {
					sendData[i][j] = 1; //サーバー送信用に船の位置（自機）は全て1に置き換え
				} else {
					sendData[i][j] = playerData[i][j];
				}
			}
		}
		RogData sendRog = new RogData("map", sendData);

		Main.sendRogData(sendRog);
	}

	/**
	 * 選択された座標から壁等の位置を判別
	 * そこからランダムで縦か横に向いた状態で3マス艦をセットするためのもの
	 * 本当は全部指定してもらう方が賢そうだがもうめんどい
	 */
	public void makeGameData() {
		Random r = new Random();
		int check = 0;
		for (int i = 0; i < playerData.length; i++) {
			for (int j = 0; j < playerData[i].length; j++) {
				if (playerData[i][j] == 2) {
					int heiWid = r.nextInt(2); //縦は0、横は1
					check = 0;

					if (heiWid == 0) {
						check = checkYData(i, j);
						if (check < 2) {
							heiWid = 1;
							check = checkXData(i, j);
						}
					} else if (heiWid == 1) {
						check = checkXData(i, j);
						if (check < 2) {
							heiWid = 0;
							check = checkYData(i, j);
						}
					}

					//縦処理（基礎）
					if (heiWid == 0 && check == 2) {
						playerData[i][j - 1] = 4;
						playerData[i][j + 1] = 4;
					} else if (heiWid == 0 && check == 3) { //（上がない、指定箇所が一番上の列）
						playerData[i][j + 1] = 4;
						playerData[i][j + 2] = 4;
					} else if (heiWid == 0 && check == 4) { //（下がない、指定箇所が一番下の列）
						playerData[i][j - 1] = 4;
						playerData[i][j - 2] = 4;
					}
					//横処理（基礎）
					else if (heiWid == 1 && check == 2) {
						playerData[i - 1][j] = 4;
						playerData[i + 1][j] = 4;
					} else if (heiWid == 1 && check == 3) { //（左がない、指定箇所が一番左の列）
						playerData[i + 1][j] = 4;
						playerData[i + 2][j] = 4;
					} else if (heiWid == 1 && check == 4) { //（右がない、指定箇所が一番右の列）
						playerData[i - 1][j] = 4;
						playerData[i - 2][j] = 4;
					}
				}

				else if (playerData[i][j] == 3) {

				}
			}
		}
		convShipCode2Data();
	}

	/**
	 * サーバーに送信用にデータを加工する関数
	 */
	public void convShipCode2Data() {
		for (int i = 0; i < playerData.length; i++) {
			for (int j = 0; j < playerData[i].length; j++) {
				if (playerData[i][j] == 4) {
					playerData[i][j] = 2; //一時的に4にしたものを2に戻す
				}
			}
		}
	}

	/**
	 * X方向に障害物（壁、自分の戦艦など）がないかを3マス艦を指定した位置からチェックする
	 *
	 * @param x		座標（チェックしたい）
	 * @param y		xと同じ
	 * @return		intフラグによるリターン形式（1：1マスのみ横無効,2：ノーマル,3：右2つ,4：左2つ）
	 */
	public int checkXData(int x, int y) {
		int flag = 0;
		if (x > 1 && x < playerData.length - 2) { //端っこではない
			if (playerData[x + 1][y] == 0) { //右隣
				flag = 1;
				if (playerData[x - 1][y] == 0) { //左隣
					flag = 2;
				}
			}
			if (flag == 1) {
				if (playerData[x + 2][y] == 0) { //右2つ隣
					flag = 3;
				}
			}
			if (flag == 0) {
				if (playerData[x - 1][y] == 0) {//左隣
					flag = 1;
					if (playerData[x - 2][y] == 0) { //左2つ隣
						flag = 4;
					}
				}
			}
		} else if (x == 1) { //左が1マスしかない
			if (playerData[x + 1][y] == 0) { //右隣
				flag = 1;
			} else if (playerData[x - 1][y] == 0) { //左側
				flag = 0;
			}
			if (flag == 1) {
				if (playerData[x + 2][y] == 0) { //右2つ隣
					flag = 3;
				}
			}
		} else if (x == playerData.length - 2) { //右が1マスしかない
			if (playerData[x - 1][y] == 0) { //左側
				flag = 1;
			} else if (playerData[x + 1][y] == 0) { //右隣
				flag = 0;
			}
			if (flag == 1) {
				if (playerData[x - 2][y] == 0) { //左2つ隣
					flag = 4;
				}
			}
		} else if (x == 0) { //右オンリー
			if (playerData[x + 1][y] == 0) { //右隣
				flag = 1;
				if (playerData[x + 2][y] == 0) { //右2つ隣
					flag = 3;
				}
			}
		} else if (x == playerData.length - 1) { //左オンリー
			if (playerData[x - 1][y] == 0) { //左隣
				flag = 1;
				if (playerData[x - 2][y] == 0) { //左2つ隣
					flag = 4;
				}
			}
		}
		return flag;
	}

	/**
	 * Y方向に障害物（壁、自分の戦艦など）がないかを3マス艦を指定した位置からチェックする
	 *
	 * @param x		確認したい座標
	 * @param y		x同様
	 * @return		intフラグによるリターン形式（1：1マスのみ縦無効,2：ノーマル,3：下2つ,4：上2つ）
	 */
	public int checkYData(int x, int y) {
		int flag = 0;
		if (y > 1 && y < playerData[x].length - 2) { //端っこではない
			if (playerData[x][y + 1] == 0) { //下
				flag = 1;
				if (playerData[x][y - 1] == 0) { //上
					flag = 2;
				}
			}
			if (flag == 1) {
				if (playerData[x][y + 2] == 0) { //下2つ
					flag = 3;
				}
			}
			if (flag == 0) {
				if (playerData[x][y - 1] == 0) {//上
					flag = 1;
					if (playerData[x][y - 2] == 0) { //上2つ
						flag = 4;
					}
				}
			}
		} else if (y == 1) { //上が1マスしかない
			if (playerData[x][y + 1] == 0) { //下
				flag = 1;
			} else if (playerData[x][y - 1] == 0) { //上
				flag = 0;
			}
			if (flag == 1) {
				if (playerData[x][y + 2] == 0) { //下2つ
					flag = 3;
				}
			}
		} else if (y == playerData[x].length - 2) { //下が1マスしかない
			if (playerData[x][y - 1] == 0) { //上
				flag = 1;
			} else if (playerData[x][y + 1] == 0) {
				flag = 0;
			}
			if (flag == 1) {
				if (playerData[x][y - 2] == 0) { //上2つ
					flag = 4;
				}
			}
		} else if (y == 0) { //下オンリー
			if (playerData[x][y + 1] == 0) { //下
				flag = 1;
				if (playerData[x][y + 2] == 0) { //下2つ
					flag = 3;
				}
			}
		} else if (y == playerData[x].length - 1) { //上オンリー
			if (playerData[x][y - 1] == 0) { //上
				flag = 1;
				if (playerData[x][y - 2] == 0) { //上2つ
					flag = 4;
				}
			}
		}

		return flag;
	}

	public boolean checkMyShipFlag() {
		int count = 0;
		for (int i = 0; i < playerData.length; i++) {
			for (int j = 0; j < playerData[i].length; j++) {
				if (playerData[i][j] > 0 && playerData[i][j] < 4) {
					count++;
				}
			}
		}
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

}
