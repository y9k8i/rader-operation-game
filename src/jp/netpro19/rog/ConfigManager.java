package jp.netpro19.rog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;

public final class ConfigManager {
	public static final Properties prop = new Properties();
	private String filepath;

	public ConfigManager(String filepath) {
		this.filepath = filepath;
		// プロパティ読み込み
		readProperty();
	}

	private final void readProperty() {
		try {
			File file = new File(filepath);
			if (file.createNewFile()) {// すでに存在する場合はfalseになるだけ
				prop.setProperty("version", "0.1");
			}
			InputStreamReader isr = new InputStreamReader(new FileInputStream(filepath), "UTF-8");
			prop.load(isr);
			setDefaultProperty();
			System.out.println("プロパティバージョン: " + prop.getProperty("version"));
		} catch (IOException e) {
			System.err.println("プロパティ読み込みエラー");
		}
	}

	private void setDefaultProperty() {
		if(prop.getProperty("version") == null)
			System.err.println("プロパティバージョンエラー: 設定されていません");
		if(prop.getProperty("servername") == null)
			prop.setProperty("servername", "NONAME");
		if(prop.getProperty("motd") == null)
			prop.setProperty("motd", "A ROG server.");
	}

	public final void writeProperty() {
		try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(filepath), "UTF-8")) {
			prop.store(osw, "ゲームの設定ファイル");
		} catch (IOException e) {
			System.err.println("プロパティ書き込みエラー");
		}
	}

}
