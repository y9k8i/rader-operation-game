package jp.netpro19.rog;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import jp.netpro19.ServerData;

public class IpSelectScreenController implements Initializable {

	static boolean isVisible = false;
	private RogData rog;
	private String serverName;
	private String description;
	private String address;
	private ArrayList<String> List = new ArrayList<String>();

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TableView<ServerData> ServerNameTable;

	@FXML
	private TableColumn<ServerData, String> ServerNameColumn;

	@FXML
	private Label infoLabel1;

	@FXML
	private Button ConnectButton;

	@FXML
	private Label infoLabelDescription;

	@FXML
	private Button backButton;

	@FXML
	void OnBackClick(ActionEvent event) {
		isVisible = false;
		new Main().changeView("TitleScreen.fxml", "application.css");
	}

	@FXML
	void onConnectButtonClick(ActionEvent event) {
		new Main().popViewSelect("ここに接続しますか？", "", 0);
		isVisible = false;
	}

	@FXML
	void onMouseConnectButton(MouseEvent event) {

	}

	public void addList(ServerInfo data) {
		if (!data.getAddress().matches("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.255") && !data.getAddress().matches("127.0.0.1")) {
			boolean addressInclude = false;
			for (String address : List) {
				if (address.equals(data.getAddress())) {
					addressInclude = true;
				}
			}
			if (!addressInclude) {
				ServerNameTable.getItems().add(new ServerData(data.getName(), data.getDescription(), data.getAddress(),
						String.valueOf(data.getPingtime())));
				List.add(data.getAddress());
			}
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ
		ServerNameColumn.setCellValueFactory(new PropertyValueFactory<>("serverName"));

		//表のクリック時の操作
		ServerNameTable.getSelectionModel().selectedItemProperty().addListener((Observable, oldVal, newVal) -> {
			serverName = newVal.serverNameProperty().getValue();
			description = newVal.descriptionProperty().getValue();
			address = newVal.addressProperty().getValue();

			infoLabel1.setText(serverName);
			infoLabelDescription.setText("説明：" + description + "\nアドレス：" + address + "\nping：");

			ConnectButton.setVisible(true);

			System.out.println(newVal.addressProperty().getValue());
		});

		new Thread(() -> {
			rog = new RogData("query", "null");
			while (isVisible && !Main.getStartGameFlag() && !Main.data.getSetupFlag()) {
				Main.servent.sendData(rog);
				try {
					Thread.sleep((long) (2000));
				} catch (InterruptedException e) {
					e.printStackTrace();
					break;
				}
			}
		}).start();

	}

	public void setIsVisible(boolean flag) {
		isVisible = flag;
	}

	public String getAddress() {
		return address;
	}

	@FXML
	void initialize() {
		assert ServerNameTable != null : "fx:id=\"ServerNameTable\" was not injected: check your FXML file 'IpSelectScreen.fxml'.";
		assert ServerNameColumn != null : "fx:id=\"NameColumn\" was not injected: check your FXML file 'IpSelectScreen.fxml'.";
		assert infoLabel1 != null : "fx:id=\"infoLabel1\" was not injected: check your FXML file 'IpSelectScreen.fxml'.";
		assert ConnectButton != null : "fx:id=\"ConnectButton\" was not injected: check your FXML file 'IpSelectScreen.fxml'.";
		assert infoLabelDescription != null : "fx:id=\"infoLabelDescription\" was not injected: check your FXML file 'IpSelectScreen.fxml'.";
		assert backButton != null : "fx:id=\"backButton\" was not injected: check your FXML file 'IpSelectScreen.fxml'.";

	}
}
