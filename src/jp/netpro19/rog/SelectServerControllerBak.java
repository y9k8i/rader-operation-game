package jp.netpro19.rog;

import java.net.SocketException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.VBox;

public class SelectServerControllerBak {

	static boolean isVisible = true;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private VBox serverListVbox;

	@FXML
	private TextField directServerAddressField;

	@FXML
	private ProgressIndicator directGoButtonProgress;

	@FXML
	private Button directGoButton;

	@FXML
	void onBackButtonAction(ActionEvent event) {
		isVisible = false;
		new Main().changeView("TitleView.fxml", "application.css");
	}

	@FXML
	void onDirectGoButtonAction(ActionEvent event) {
		// TODO
	}

	@FXML
	void onServerAddressFieldChanged(InputMethodEvent event) {
		directGoButton.setDisable(true);
		directGoButtonProgress.setVisible(true);
		// サーバに到達できる場合
		if(Main.servent.ping(directServerAddressField.getText()))
			directGoButton.setDisable(false);
		directGoButtonProgress.setVisible(false);
	}

	public void addVbox(ServerInfo sInfo) {
		ServerInfoControl serverInfoControl = new ServerInfoControl();
		serverInfoControl.setServerTitle(sInfo.getName());
		serverInfoControl.setserverAddress(sInfo.getAddress());
		serverInfoControl.setGoButtonTooltip("ツールチップ");
		Platform.runLater(() -> serverListVbox.getChildren().add(serverInfoControl));
	}

	public void setDirectServerAddressField(String string) {
		this.directServerAddressField.setText(string);
	}

	@FXML
	void initialize() {
		assert serverListVbox != null : "fx:id=\"serverListVbox\" was not injected: check your FXML file 'SelectServerView.fxml'.";
		assert directServerAddressField != null : "fx:id=\"directServerAddressField\" was not injected: check your FXML file 'SelectServerView.fxml'.";
		assert directGoButtonProgress != null : "fx:id=\"directGoButtonProgress\" was not injected: check your FXML file 'SelectServerView.fxml'.";
		assert directGoButton != null : "fx:id=\"directGoButton\" was not injected: check your FXML file 'SelectServerView.fxml'.";
		new Thread(()  -> {
			while(isVisible) {
				try {Main.servent.queryServer();} catch (SocketException e1) {e1.printStackTrace();break;}
				try {	Thread.sleep((long) (2000));} catch (InterruptedException e) {e.printStackTrace();break;}
			}
		}).start();
	}
}
