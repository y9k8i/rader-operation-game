package jp.netpro19.rog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PopScreenController implements Initializable {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private VBox messageDisp;

	@FXML
	private Label messageLable;

	@FXML
	private Button OKButton;

	@FXML
	void onClicked(ActionEvent event) {
		String fxml = Main.getFxmlName();
		if (fxml.equals("")) {
			//以下ウィンドウクローズ処理（https://codeday.me/jp/qa/20190123/157042.html参照）
			Stage stage = (Stage) OKButton.getScene().getWindow();
			stage.close();
		} else if (fxml.equals("GameMy_ScreenMain.fxml")) {
//			if (MainR.data.getTurn()) {
//				MainR.data.setTurn(false);
//			} else {
//				MainR.data.setTurn(true);
//			}
			new Main().changeView(fxml, "application.css");
			Stage stage = (Stage) OKButton.getScene().getWindow();
			stage.close();
		} else if (fxml.equals("end")) {
			Stage stage = (Stage) OKButton.getScene().getWindow();
			stage.close();
			Platform.exit();
			Main.endGame();
		} else {
			new Main().changeView(fxml, "application.css");
			Stage stage = (Stage) OKButton.getScene().getWindow();
			stage.close();
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ
		String message = Main.getText();
		messageLable.setText(message);
	}

	@FXML
	void initialize() {
		assert messageDisp != null : "fx:id=\"messageDisp\" was not injected: check your FXML file 'PopScreen.fxml'.";
		assert messageLable != null : "fx:id=\"messageLable\" was not injected: check your FXML file 'PopScreen.fxml'.";
		assert OKButton != null : "fx:id=\"OKButton\" was not injected: check your FXML file 'PopScreen.fxml'.";

	}
}
