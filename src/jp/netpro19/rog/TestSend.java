package jp.netpro19.rog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 送信テスト用クラス ブロードキャストにも対応 チャットとフィールド両対応
 */
public class TestSend {

	private static RogData data = null;

	public static void main(String[] args) {

		int[][] testArray = { { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 },
				{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 },
				{ 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 }, { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
				{ 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024 }, { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
				{ 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 }, { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } };

		/** ブロードキャスト */
		BroadcastServent broadcastServent = null;
		/** マルチキャスト */
		MulticastServent multicastServent = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		boolean useBroadcast = true;
		try {
			System.out.print("マルチキャストかブロードキャストかを入力[m(ulticast), b(roadcast), quit]: ");
			line = reader.readLine();
			switch (line) {
			case "m":
			case "multicast":
				System.out.println("マルチキャストに設定されました");
				useBroadcast = false;
				break;
			case "b":
			case "broadcast":
				System.out.println("ブロードキャストに設定されました");
				useBroadcast = true;
				break;
			default:
				System.err.println("判別不可: " + line);
				System.err.println("ブロードキャストに設定します");
			}

			if(useBroadcast)
				broadcastServent = new BroadcastServent();
			else
				multicastServent = new MulticastServent();

			while (true) {
				System.out.print("操作を入力[chat, array, quit]: ");
				line = reader.readLine();
				if (line.equals("quit"))
					break;
				switch (line) {
				case "chat":
					System.out.println("送信するメッセージ: ");
					line = reader.readLine();
					data = new RogData("chat", line);
					if(useBroadcast)
						broadcastServent.sendData(data);
					else
						multicastServent.sendData(data);
					break;
				case "array":
					data = new RogData("map", testArray);
					if(useBroadcast)
						broadcastServent.sendData(data);
					else
						multicastServent.sendData(data);
					break;
				default:
					System.err.println("不正な操作: " + line);
				}
				System.out.println("送信しました");
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
