package jp.netpro19.rog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class GameMyScreenMainConroller implements Initializable {
	private final int SPACE = 5;
	private final int X = 10;
	private final int Y = 10;
	private int[][] shipFlag; //船があるか管理するフラグ
	boolean myTurn = false;
	boolean chatFlag;
	private RogData rogData;
	private String text = "";

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private GridPane GameField;

	@FXML
	private Label statusLabel;

	@FXML
	private Label explainLabel;

	@FXML
	private ScrollPane scroll;

	@FXML
	private VBox message_display;

	@FXML
	private TextField ChatField;

	@FXML
	private Button sendButton;

	@FXML
	private Button btn[][];

	@FXML
	void sendAction(ActionEvent event) {
		//呼び出されたコントローラ
		Object source = event.getSource();
		//ボタンにキャストできるなら
		if (source instanceof Button) {
			//ボタンのIDを取得して引数にする
			sendMessage(((Button) source).getId());
		}

	}

	public void sendMessage(String message) { //サンプル参考
		text = message;
		sendChatPane(ChatField);
	}

	private void sendChatPane(TextField message) { //サンプルから引っ張ってきたもの

		Label label = new Label();

		//チャットテキストが空でないなら
		if (!message.getText().equals("")) {
			if (message.getText().contains("Log：")) {
				message.setText("Log：で始まる文は送信できません");
				label = new Label(message.getText());
				label.setId("chat");
				message.setText("");
				message_display.getChildren().add(label);
				scroll.setVvalue(1.);
			} else {
				//チャットテキストを表示させるLabelを作る
				label = new Label(message.getText());

				//ネットワークで送信するにはここらへんに追記必須
				if (!Main.data.getSetupFlag()) {
					rogData = new RogData("chat", message.getText());
					Main.sendRogData(rogData);
				}

				//LabelにIDを設定して、Application2.css内のcssを適用させる
				label.setId("chat");
				//チャット入力欄の文字を消す
				message.setText("");
			}

		} else if (Main.getChatFlag()) {

			if (text.contains("Log：")) {
				if (text.contains(Main.servent.getMyHostAddress() + "のターンです")) {
					text = "Log：あなたのターンです";
				}
			}

			//チャットテキストを表示させるLabelを作る
			label = new Label(text);

			//AnchorPaneの左上の座標から見て、SPACE分だけ下にLabelをずらす
			label.setLayoutY(SPACE);
			//LabelにIDを設定して、Application2.css内のcssを適用させる
			label.setId("chat");

			System.out.println("受信しました");
			Main.setChatFlag(false);

			//VBoxに追加
			message_display.getChildren().add(label);

			//スクロールを後方に移動
			scroll.setVvalue(1.);
		}

	}

	@Override
	//パネルのボタン初期化（https://yamakasa3.hatenablog.com/entry/2018/07/16/120000一部参照）
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ
		btn = new Button[X][Y]; // (列,行)
		message_display.getChildren().addAll(Main.data.getChatBox());
		myTurn = Main.data.getTurn(); // 自分の番かどうか取得
		if (myTurn) {
			statusLabel.setText("自分のターンです");
			explainLabel.setText("攻撃したいマスを選択");

			shipFlag = Main.getGameData(1);
		} else {
			statusLabel.setText("相手のターンです");
			explainLabel.setText("詳細はチャット欄にも届きます");
			shipFlag = Main.getGameData(0);
		}

		for (int i = 0; i < btn.length; i++) {
			for (int j = 0; j < btn[i].length; j++) {
				AnchorPane panel = new AnchorPane();
				btn[i][j] = new Button();
				btn[i][j].setPrefWidth(10);
				btn[i][j].setPrefHeight(10);
				btn[i][j].setAlignment(Pos.CENTER);
				btn[i][j].setMinHeight(0.0);
				btn[i][j].setMinWidth(0.0);
				String num = String.valueOf(i + 1) + "," + String.valueOf(j + 1); //id設定用
				btn[i][j].setId("selectPane" + num);
				btn[i][j].setOnAction(event -> onClickPane(event));
				btn[i][j].setOnContextMenuRequested(event -> onRightClickPane(event));
				if (shipFlag[i][j] == 1 && !myTurn)
					btn[i][j].setStyle("-fx-base: red");
				else if (shipFlag[i][j] == 2 && !myTurn)
					btn[i][j].setStyle("-fx-base: green");
				else if (shipFlag[i][j] == 3 && !myTurn)
					btn[i][j].setStyle("-fx-base: blue");
				else if (shipFlag[i][j] == 10 && !myTurn)
					btn[i][j].setStyle("-fx-base: black");
				else if (shipFlag[i][j] >= 4 && myTurn)
					btn[i][j].setStyle("-fx-base: red");// マーク済みフラグ
				else if (shipFlag[i][j] >= 2 && myTurn)
					btn[i][j].setStyle("-fx-base: black");// 一度クリック済みフラグ
				panel.setPrefSize(200.0, 200.0);
				AnchorPane.setBottomAnchor(btn[i][j], 2.0);
				AnchorPane.setTopAnchor(btn[i][j], 1.0);
				AnchorPane.setLeftAnchor(btn[i][j], 1.0);
				AnchorPane.setRightAnchor(btn[i][j], 2.0);
				panel.getChildren().add(btn[i][j]);
				GridPane.setConstraints(panel, i, j);
				GameField.getChildren().add(panel);

			}
		}
	}

	public void onRightClickPane(ContextMenuEvent e) {
		if (myTurn) {
			InputFromatter idCoord = new InputFromatter(((Button) e.getSource()).getId()); //idから座標の抽出用
			int num[] = idCoord.convString();
			System.out.println("クリックしたのは、" + num[0] + "," + num[1] + "デスネ");
			int x = num[0] - 1;
			int y = num[1] - 1;

			if (shipFlag[x][y] < 4 && btn[x][y].getStyle().equals("")) {
				shipFlag[x][y] += 4;
				btn[x][y].setStyle("-fx-base: red");
			} else if (shipFlag[x][y] >= 4 && btn[x][y].getStyle().equals("-fx-base: red")) {
				shipFlag[x][y] -= 4;
				btn[x][y].setStyle("");
			}
			Main.setGameData(1, shipFlag);
		}
	}

	public void onClickPane(ActionEvent event) {
		if (myTurn) {
			InputFromatter idCoord = new InputFromatter(((Button) event.getSource()).getId()); //idから座標の抽出用
			int num[] = idCoord.convString();
			System.out.println("クリックしたのは、" + num[0] + "," + num[1] + "デスネ");
			int x = num[0] - 1;
			int y = num[1] - 1;

			if (shipFlag[x][y] < 2 && btn[x][y].getStyle().equals("")) {
				shipFlag[x][y] += 2;
			} else if (shipFlag[x][y] >= 4 && btn[x][y].getStyle().equals("-fx-base: red")) {
				shipFlag[x][y] -= 2;
			}
			btn[x][y].setStyle("-fx-base: yellow");
			Main.setGameData(1, shipFlag);

			RogData rog = new RogData("shot", x + "," + y);
			Main.sendRogData(rog);
		}
	}

	public void setChatBox() { //シーン入れ替わり時に勝手にバックアップさせるためのもの
		Main.setChatBox(message_display);
	}

	@FXML
	void initialize() {
		assert GameField != null : "fx:id=\"GameField\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";
		assert statusLabel != null : "fx:id=\"statusLabel\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";
		assert explainLabel != null : "fx:id=\"explainLabel\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";
		assert scroll != null : "fx:id=\"scroll\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";
		assert message_display != null : "fx:id=\"message_display\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";
		assert ChatField != null : "fx:id=\"ChatField\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";
		assert sendButton != null : "fx:id=\"sendButton\" was not injected: check your FXML file 'GameMy_ScreenMain.fxml'.";

	}
}
