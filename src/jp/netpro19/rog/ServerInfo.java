package jp.netpro19.rog;
import java.io.Serializable;

import javafx.scene.image.Image;

/**
 * サーバ情報を格納するクラス。
 */
@SuppressWarnings("serial")
public class ServerInfo implements Serializable {
	private String name;
	private String description;
	private String address;
	private int pingtime = -1;
	private Image image = null;

	public ServerInfo(String name, String description) {
		setName(name);
		setDescription(description);
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		String result = "サーバ名: " + name + "\nサーバ説明: " + description;
		if(pingtime > 0)
			result += "\nping: " + pingtime + "ms";
		return result;
	}

	@Override
	public boolean equals(Object o){
		if(((ServerInfo) o).getName() == getName() && ((ServerInfo) o).getDescription() == getDescription())
			return true;
		return false;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPingtime() {
		return pingtime;
	}

	public void setPingtime(int pingtime) {
		this.pingtime = pingtime;
	}

}
