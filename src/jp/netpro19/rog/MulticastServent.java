package jp.netpro19.rog;

import static jp.netpro19.rog.Main.DEBUG;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * ネットワークに関わることは全部このクラスが担当する。<br>
 * ２回以上インスタンス化してもいいことになった。<br>
 * マルチキャスト通信は<a href=
 * "https://nompor.com/2018/12/04/post-3742/">このページ</a>を参考にした<br>
 * シリアライズ・デシリアライズは<a href=
 * "http://tsukaayapontan.web.fc2.com/doc/serialize/serialize.html">このページ</a>を参考にした<br>
 * 整数とバイト配列の相互変換は<a href=
 * "https://qiita.com/nwtgck/items/c53db76e19ef80296a71">このページ</a>を参考にした
 */
public class MulticastServent {

	/**
	 * ROGをUnicodeでエンコードしたU+52U+4FU+47から。<br>
	 * 49152から65535は自由に使える。
	 */
	private static final int PORT = 52447;
	static final String GROUPADDRESS = "224.52.4.47";
	public static MulticastSocket socket;
	private ByteArrayOutputStream bos = null;
	private ObjectOutputStream oos = null;
	private ByteArrayInputStream bis = null;
	private ObjectInputStream ois = null;
	private DatagramPacket packet = null;
	private InetAddress group;
	/** 送信元IPアドレス */
	private String hostaddress = null;
	/** 時間をミリ秒で格納 */
	private static long timems;
	private static int pingms;

	// データ
	byte[] data = null;
	byte[] lengthbuf = new byte[4];
	static RogData pongdata = null;
	int length;
	private static boolean initFlag = false;
	private static boolean receiving = false;
	/** 設定中フラグ この間は受信を行わない */
	static boolean setting = false;
	/** 重複チェック用 画面に追加する処理は別に必要 */
	static ArrayList<ServerInfo> servers = new ArrayList<ServerInfo>();

	/** すべてのサーバの情報を取得する */
	public void queryServer() throws SocketException {
		Main.sendRogData(new RogData("query", null));
	}

	public MulticastServent() {
		if (DEBUG)
			System.out.println(this.getClass().getName() + "インスタンス化");
		if (initFlag == false) {
			init();
			initFlag = true;
		}
	}

	/**
	 * 初期化をするメソッド<br>
	 * ソケットを作る時にのみ呼び出す
	 */
	private void init() {
		try {
			socket = new MulticastSocket(PORT);

			Enumeration<NetworkInterface> nInterfaces = NetworkInterface.getNetworkInterfaces();
			NetworkInterface nInterface;
			while (nInterfaces.hasMoreElements()) {
				nInterface = (NetworkInterface) nInterfaces.nextElement();
				if (nInterface.getName().equals("en0")) {
					System.out.println("ネットワークインタフェースen0を見つけたためインターフェースen0に設定します");
					socket.setNetworkInterface(nInterface);
					break;
				}
			}

			socket.setLoopbackMode(false); // 自分が送信する内容を受信しないようにする
			group = InetAddress.getByName(GROUPADDRESS);
			socket.joinGroup(group);
		} catch (IOException e) {
			System.err.println(this.getClass().getName() + "のネットワーク初期化に失敗しました");
			e.printStackTrace();
		}
	}

	/**
	 * データを受信するメソッド。一つ受信したら終了する。<br>
	 * <font color="red">
	 */
	public RogData recvData() {
		if (receiving) {
			System.err.println("エラー: 受信メソッド多重起動\n\tat " + ThreadUtils.calledFrom());
			return null;
		} else {
			receiving = true;
			RogData obj = null;
			try {
				// フェーズ1 これから来るデータのバイト数をintで受け取る
				packet = new DatagramPacket(lengthbuf, 4);
				socket.receive(packet);
				length = ByteBuffer.wrap(lengthbuf).getInt();
				hostaddress = packet.getAddress().getHostAddress().toString();
				if (DEBUG)
					System.out.println(hostaddress + "から" + length + "バイトのデータを受信します");
				// フェーズ2 データを受信する
				data = new byte[length];
				packet = new DatagramPacket(data, length);
				socket.receive(packet);
				bis = new ByteArrayInputStream(data);
				ois = new ObjectInputStream(bis);
				obj = (RogData) ois.readObject();
				if (DEBUG)
					System.out.println("受信するデータは: " + obj);
			} catch (SocketTimeoutException e) {
				if (DEBUG)
					System.out.println("受信待機ソケットがタイムアウトしました");
			} catch (SocketException e) {
				if (DEBUG)
					System.out.println("受信待機ソケットが閉じました");
			} catch (IOException | ClassNotFoundException e) {
				System.err.println(this.getClass().getName() + "内でエラーが発生しました");
				e.printStackTrace();
			} finally {
				receiving = false;
				try {
					if (bis != null)
						bis.close();
					if (ois != null)
						ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return obj;
		}
	}

	/**
	 * データを送信するメソッド。スレッドとして起動させる。
	 */
	public void sendData(RogData obj) {
		try {
			bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			// フェーズ1 これから送信するデータをバイト型配列にする
			oos.writeObject(obj);
			data = bos.toByteArray();
			// フェーズ2 これから送信するデータのバイト数をintで送る
			length = data.length;
			if (DEBUG)
				System.out.println(length + "バイトのデータを送信します\n\tat " + ThreadUtils.calledFrom());
			lengthbuf = ByteBuffer.allocate(4).putInt(length).array();
			packet = new DatagramPacket(lengthbuf, 4, group, PORT);
			socket.send(packet);
			// フェーズ3 データを送信する
			if (DEBUG)
				System.out.println("送信するデータは: " + obj);
			packet = new DatagramPacket(data, length, group, PORT);
			socket.send(packet);
		} catch (IOException e) {
			System.err.println(this.getClass().getName() + "内でエラーが発生しました");
			e.printStackTrace();
		} finally {
			try {
				if (bos != null)
					bos.close();
				if (oos != null)
					oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * タイムアウト時間を設定する 受信時のみ有効
	 *
	 * @param ms ミリ秒 0で無効になる
	 */
	public static void setTimeout(int ms) {
		if (DEBUG)
			System.out.println("受信タイムアウトが" + ms + "msに設定されます\n\tat " + ThreadUtils.calledFrom());
		if (DEBUG && !MulticastServent.setting)
			System.err.println("settingが偽のため反映されません！");
		try {
			socket.setSoTimeout(ms);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public boolean ping(String address) {
		boolean result = false;
		pingms = -1;
		if (DEBUG)
			System.out.println(address + "へのpingスタート \n\tat " + ThreadUtils.calledFrom());
		pongdata = null;
		// ここから順番大事
		setting = true;
		socket.close();
		init();
		setTimeout(500);
		timems = System.currentTimeMillis();
		Main.sendRogData(new RogData("ping", address));
		setting = false;
		while (System.currentTimeMillis() - timems < 500) {
			if (pongdata == null)
				continue;
			else if (pongdata.getType() == "pong" && pongdata.getData() == address) {
				pingms = (int) (System.currentTimeMillis() - timems);
				result = true;
				break;
			} else
				System.out.println("pong以外のデータを受信しました 継続してpongの受信を試みます");
		}
		// ここから順番大事
		setting = true;
		socket.close();
		init();
		setTimeout(0);
		setting = false;
		return result;
	}

	/** 直前に受信したデータのIPアドレスを取得 */
	public String getHostAddress() {
		return hostaddress;
	}

	/** 自分のIPアドレスを取得 */
	public String getMyHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			System.err.println("自分のIPアドレスを取得できません\n\tat " + ThreadUtils.calledAt());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 直前に実行したpingのミリ秒を取得<br>
	 * ping失敗時は-1が返る
	 */
	public static int getPingms() {
		return pingms;
	}

	public void interrupt() {
		try {
			if (DEBUG)
				System.out.println(this.getClass().getName() + "の割り込み処理開始");
			socket.leaveGroup(group);
			socket.close();
		} catch (IOException e) {
			System.out.println(this.getClass().getName() + "内でエラー発生");
			e.printStackTrace();
		}
	}

}
