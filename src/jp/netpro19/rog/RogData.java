package jp.netpro19.rog;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 通信時にやり取りされるクラスオブジェクト。つまりキャスト不可なら捨てていい。 盤面=645バイト 500文字のチャットテキスト≒1600バイトちょい
 *
 * @param type データのタイプ。インスタンス化する時に決定し書き換え不可。ゲッターがある。
 * @param data データ本体。ゲッターおよびセッターがある。
 */
public class RogData implements Serializable {
	// 変更したら番号振り直す あまり意味は理解出来ていない
	private static final long serialVersionUID = -3757148070492145525L;
	private String type = null;
	private Object data = null;

	public RogData(String type, Object data) {
		this.type = type;
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "RogData {type=" + type + ", data=" + dumpInstanceFields(data) + "}";
	}

	/**
	 * int[][]と文字列にできる型を区別して文字列にしてくれるメソッド。<br>
	 * また無駄な時間を使ってしまった…（このメソッド作るのに２時間くらいかかった）
	 */
	public static String dumpInstanceFields(Object object) {
		if(object == null)
			return "null";
		else if (object instanceof int[][]) {
			return "\n" + Arrays.deepToString((int[][]) object).replaceAll(" \\[", "\n[") + "\n";
		} else
			return object.toString();
	}
}
