package jp.netpro19.rog;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

public class TitleScreenController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField textFieldId;

	@FXML
	private Button Button_GO;

	@FXML
	private Button ButtonSelect;

	@FXML
	void onSelectButtonClick(ActionEvent event) {
		new Main().changeView("IpSelectScreen.fxml", "application.css");
	}

	@FXML
	protected void onButtonClick(ActionEvent evt) {
		InputFromatter checkInput = new InputFromatter(textFieldId.getText());
		Optional<ButtonType> result;
		if (checkInput.checkIpFormat()) {

			//サーバーに接続をここで行い、タイムアウト時にはエラーを表示
			Main.address = textFieldId.getText();
			RogData rog = new RogData("ping", textFieldId.getText());
			Main.sendRogData(rog);

		} else {
			//規格外の入力をはじめから弾くため、弾いた時の処理
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("無効なIPアドレス");
			alert.setContentText("アドレスに到達できません");
			result = alert.showAndWait();
			if (result.isPresent() && result.get() == ButtonType.OK) { //確認用
				System.out.println("クリックアクション受け取り");
			}
		}

	}

	@FXML
	void initialize() {
		assert textFieldId != null : "fx:id=\"textFieldId\" was not injected: check your FXML file 'TitleScreen.fxml'.";
		assert Button_GO != null : "fx:id=\"Button_GO\" was not injected: check your FXML file 'TitleScreen.fxml'.";
		assert ButtonSelect != null : "fx:id=\"ButtonSelect\" was not injected: check your FXML file 'TitleScreen.fxml'.";

	}
}
