package jp.netpro19.rog;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

public class ServerInfoControl extends AnchorPane {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private Text serverTitle;

	@FXML
	private ProgressIndicator progress;

	@FXML
	private Button goButton;

	@FXML
	private Tooltip goButtonTooltip;

	@FXML
	void onGoButtonAction(ActionEvent event) {

	}

	private String serverAddress;

	public void setServerTitle(String serverTitle) {
		this.serverTitle.setText(serverTitle);
	}

	public void setGoButtonTooltip(String string) {
		this.goButtonTooltip.setText(string);
	}

	public void setserverAddress(String address) {
		serverAddress = address;
	}

	public ServerInfoControl() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ServerInfo.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		progress.setVisible(true);
		// サーバにping
		if(Main.servent.ping(serverAddress)) {
			goButton.setDisable(false);
			setGoButtonTooltip(BroadcastServent.getPingms() + "ms");
		} else
			setGoButtonTooltip("ping失敗");
		progress.setVisible(false);
	}
}
