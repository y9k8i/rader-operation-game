package jp.netpro19.rog;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {

	private static Stage stage; //前面に表示するステージと格納
	private static Stage loadStage; //ローディング用ステージ
	private static String tmpText; //ほかのコード上でのテキストやり取り用
	private static String tmpFxmlName;
	private static boolean startGameFlag = false;
	private static boolean hostFlag = false;
	private static boolean chatFlag = false;
	static boolean endFlag = false;
	static String address;
	public static GameData data;
	public static FXMLLoader loader;

	/** 設定ファイルがある場所 */
	private static final String CONFPATH = "game.properties";
	public static final boolean DEBUG = true;

	public static BroadcastServent servent = new BroadcastServent();
	public static ConfigManager conf = new ConfigManager(CONFPATH);
	private static Thread server = new BroadcastServer();

	/**
	 * ロード画面閉じるためだけ
	 */
	public static void destroyLoad() {
		loadStage.close();
	}

	public static void endGame() {
		Platform.exit();
		System.exit(0);
	}

	/**
	 * ゲームパネル作成（選択後に使用）
	 */
	public static void gamePaneSetup() {
		data.makeGameData();
		data.setSetupNum(3);
		new Main().changeView("GameMy_Screen4.fxml", "application.css");
		loadStage.close();
	}

	public static boolean getChatFlag() {
		return chatFlag;
	}

	public static String getFxmlName() { //controllerとほかのものの行き来用（多分非効率）
		return tmpFxmlName;
	}

	public static int[][] getGameData(int mode) {
		if(mode == 1) {
			return data.getOpponentData();
		}else {
			return data.getData();
		}
	}

	public static boolean getHostFlag() {
		return hostFlag;
	}

	public static boolean getStartGameFlag() {
		return startGameFlag;
	}

	public static String getText() { //controllerとほかのものの行き来用（多分非効率）
		return tmpText;
	}

	public static boolean hitCheck(int[] m) {
		if (data.getOneData(m[0], m[1], 0) < 4 && data.getOneData(m[0], m[1], 0) > 0) {
			data.setOneData(m[0], m[1], 10);
			return true;
		}else {
			return false;
		}
	}

	public static void main(String[] args) {

		// launchの寿命はアプリケーションと同じ
		launch(args);

		// ここから下は終了時に実行される
		IpSelectScreenController.isVisible = false;
		conf.writeProperty();
		server.interrupt();

		// 掃除（駆逐）
		for (Thread t : Thread.getAllStackTraces().keySet())
			if (t.getState()==Thread.State.RUNNABLE)
				t.interrupt();
	}

	/**
	 * 非UIスレッドからUIを触るための救済措置
	 * https://qiita.com/opengl-8080/items/51bef25aa05ecd929a3d
	 *
	 * @param popNumber 0:destroyPopLoad（削除用） 1:popView 2:popViewSerect 3:popViewLoad 4:ChangeWindow（ポップではない）
	 * @param message 4の場合のみcssファイル名入力
	 * @param fxml
	 */
	public static void nonUIControlPop(int popNumber, String message, String fxml) {
		new Thread(() -> {
			System.out.println("thread = " + Thread.currentThread().getName());

			Platform.runLater(() -> {
				System.out.println("runLater thread = " + Thread.currentThread().getName());

				switch (popNumber) {
				case 0:
					loadStage.close();
					break;
				case 1:
					new Main().popView(message, fxml);
					break;

				case 2:
					new Main().popViewSelect(message, fxml, 0);
					break;

				case 3:
					new Main().popViewLoad(message, fxml);
					break;

				case 4:
					new Main().changeView(fxml, message);
					break;
				}

			});
		}).start();

	}

	public static void sendRogData(RogData rogData) {
		new Thread(() -> {
			servent.sendData(rogData);
		System.out.println("送信しました");
		}).start();
	}


	public static void setChatBox(VBox box) {
		data.setChatData(box);
	}

	public static void setChatFlag(boolean flag) {
		chatFlag = flag;
	}

	/**
	 * ゲームの盤面をセットする。
	 * @param mode 1:相手譜面をセット それ以外:自分の譜面をセット
	 * @param data
	 */
	public static void setGameData(int mode, int[][] data) {
		if(mode == 1) {
			Main.data.setOpponentData(data);
		}else {
			Main.data.setPlayerData(data);
		}
	}

	public static void setHosrFlag(boolean flag) {
		hostFlag = flag;
	}

	/**
	 * ゲーム開始フラグをセットする
	 * @param flag
	 */
	public static void setStartGameFlag(boolean flag) {
		startGameFlag = flag;
	}

	public static void startServer() {
		server.start();
	}

	/**
	 * 画面推移をするメソッド。
	 * <a href="http://ginironodangan.blogspot.com/2017/09/javafx.html">このサイトを参照した</a>
	 *
	 * @param fxml 推移先のFXMLファイル名
	 * @param css	cssファイル
	 */
	public final void changeView(String fxml, String css) {
		try {
			//サーバー選択画面処理用
			if(fxml.equals("IpSelectScreen.fxml")) {
				IpSelectScreenController.isVisible = true;
			}
			loader = new FXMLLoader(getClass().getResource(fxml));
			Parent sceneLoaded = (Parent) loader.load();
			//ウィンドウの大きさなどを指定
			Scene scene = new Scene(sceneLoaded, stage.getScene().getWidth(), stage.getScene().getHeight());
			scene.getStylesheets().add(getClass().getResource(css).toExternalForm());
			stage.setScene(scene);
		} catch (IOException e) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
			System.err.println("遷移エラー");
		}
	}

	/**
	 * ポップアップウィンドウ（https://codeday.me/jp/qa/20190222/292240.html参考）
	 * @param message 	表示メッセージ
	 * @param fxml		fxmlファイル
	 */
	public void popView(String message, String fxml) {
		tmpText = message;
		tmpFxmlName = fxml;
		try {
			final Stage dialog = new Stage();
			dialog.setResizable(false); // リサイズ禁止
			dialog.initModality(Modality.APPLICATION_MODAL); //NONEでほかのウィンドウ操作をブロック
			dialog.initOwner(stage);
			Parent child = (Parent) FXMLLoader.load(getClass().getResource("PopScreen.fxml"));
			dialog.setTitle("情報");

			//×ボタン無効化（https://teratail.com/questions/45663参照）
			dialog.setOnCloseRequest((e) -> {
				System.out.println("閉じません");
				e.consume(); // consumeメソッドはEventをここでせき止めます
			});

			Scene dialogScene = new Scene(child);
			dialog.setScene(dialogScene);
			dialog.show();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("作成失敗");
		}
	}

	/**
	 * ロードポップアップウィンドウ
	 * @param message	表示メッセージ
	 * @param fxml		fxmlファイル
	 */
	public void popViewLoad(String message, String fxml) {
		tmpText = message;
		tmpFxmlName = fxml;
		try {
			loadStage = new Stage();
			loadStage.setResizable(false); // リサイズ禁止
			loadStage.initModality(Modality.APPLICATION_MODAL); //NONEでほかのウィンドウ操作をブロック
			loadStage.initOwner(stage);
			Parent child = (Parent) FXMLLoader.load(getClass().getResource("PopLoadScreen.fxml"));
			loadStage.setTitle("処理中");
			//×ボタン無効化
			loadStage.setOnCloseRequest((e) -> {
				System.out.println("閉じません");
				e.consume(); // consumeメソッドはEventをここでせき止めます
			});

			Scene dialogScene = new Scene(child);
			loadStage.setScene(dialogScene);
			loadStage.show();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("作成失敗");
		}
	}

	/**
	 * 選択できるポップアップウィンドウ
	 * @param message メッセージテキスト
	 * @param fxml 遷移したい画面ファイル名
	 * @param GameScreenMode ゲームセットアップ画面以外の時は0に
	 */
	public void popViewSelect(String message, String fxml, int GameScreenMode) {
		tmpText = message;
		tmpFxmlName = fxml;
		try {
			final Stage dialog = new Stage();
			dialog.setResizable(false); // リサイズ禁止
			dialog.initModality(Modality.APPLICATION_MODAL); //NONEでほかのウィンドウ操作をブロック
			dialog.initOwner(stage);
			Parent child = (Parent) FXMLLoader.load(getClass().getResource("PopSelectScreen.fxml"));
			dialog.setTitle("確認");
			if(GameScreenMode > 0) data.setSetupNum(GameScreenMode); //初期画面が使うためだけのもの
			Scene dialogScene = new Scene(child);
			dialog.setScene(dialogScene);
			dialog.show();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("作成失敗");
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		loader = new FXMLLoader(getClass().getResource("TitleScreen.fxml"));
		stage.setTitle("レーダー作戦ゲーム");
		Parent sceneLoaded = (Parent) loader.load();

		//ウィンドウの大きさなどを指定
		Scene scene = new Scene(sceneLoaded);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		stage.setScene(scene);

		stage.show();
		server.start();
	}
}
