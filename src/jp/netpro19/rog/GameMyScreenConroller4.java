package jp.netpro19.rog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class GameMyScreenConroller4 implements Initializable {
	private RogData rogData;
	private final int SPACE = 5;
	private final int X = 10;
	private final int Y = 10;
	private int[][] shipFlag = Main.getGameData(0); //船があるか管理するフラグ
	boolean myTurn = false;
	private String text = "";

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private GridPane GameField;

	@FXML
	private Label statusLabel;

	@FXML
	private Label explainLabel;

	@FXML
	private Button nextButton;

	@FXML
	private ScrollPane scroll;

	@FXML
	private VBox message_display;

	@FXML
	private TextField ChatField;

	@FXML
	private Button sendButton;

	@FXML
	void onClickedNext(ActionEvent event) {
		//自分の画面を表示
		rogData = new RogData("map", shipFlag);
		Main.setChatBox(message_display);
		Main.data.sendData();
		if (Main.getHostFlag())
			new Main().popViewLoad("ゲストの設定を待ちます", "GameMy_ScreenMain.fxml");
		else if (!Main.getHostFlag() && Main.data.getSetupFlag())
			Main.data.setSetupFlag(false);
		new Main().changeView("GameMy_ScreenMain.fxml", "application.css");
	}

	@FXML
	private Button btn[][];

	@FXML
	void sendAction(ActionEvent event) {
		//呼び出されたコントローラ
		Object source = event.getSource();
		//ボタンにキャストできるなら
		if (source instanceof Button) {
			//ボタンのIDを取得して引数にする
			sendMessage(((Button) source).getId());
		}

	}

	public void sendMessage(String message) { //サンプル参考
		text = message;
		setChatPane(ChatField);
	}

	private void setChatPane(TextField message) { //サンプルから引っ張ってきたもの

		Label label = new Label();

		//チャットテキストが空でないなら
		if (!message.getText().equals("")) {

			//チャットテキストを表示させるLabelを作る
			label = new Label(message.getText());

			//ネットワークで送信するにはここらへんに追記必須
			if (!Main.data.getSetupFlag()) {
				rogData = new RogData("chat", message.getText());
				Main.sendRogData(rogData);
			}

			//LabelにIDを設定して、Application2.css内のcssを適用させる
			label.setId("chat");
			//チャット入力欄の文字を消す
			message.setText("");

		} else if (Main.getChatFlag()) {

			//チャットテキストを表示させるLabelを作る
			label = new Label(text);

			//AnchorPaneの左上の座標から見て、SPACE分だけ下にLabelをずらす
			label.setLayoutY(SPACE);
			//LabelにIDを設定して、Application2.css内のcssを適用させる
			label.setId("chat");

			System.out.println("受信しました");

			//BorderPaneをVBoxに追加
			message_display.getChildren().add(label);

			//スクロールを後方に移動
			scroll.setVvalue(1.);
		}

	}

	@Override
	//パネルのボタン初期化（https://yamakasa3.hatenablog.com/entry/2018/07/16/120000一部参照）
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ
		message_display.getChildren().addAll(Main.data.getChatBox());
		btn = new Button[X][Y]; // (列,行)
		for (int i = 0; i < btn.length; i++) {
			for (int j = 0; j < btn[i].length; j++) {
				AnchorPane panel = new AnchorPane();
				btn[i][j] = new Button();
				btn[i][j].setPrefWidth(10);
				btn[i][j].setPrefHeight(10);
				btn[i][j].setAlignment(Pos.CENTER);
				btn[i][j].setMinHeight(0.0);
				btn[i][j].setMinWidth(0.0);
				String num = String.valueOf(i + 1) + "," + String.valueOf(j + 1); //id設定用
				btn[i][j].setId("selectPane" + num);
				btn[i][j].setOnAction(event -> onClickPane(event));
				if (shipFlag[i][j] == 1)
					btn[i][j].setStyle("-fx-base: red");
				else if (shipFlag[i][j] == 2)
					btn[i][j].setStyle("-fx-base: green");
				else if (shipFlag[i][j] == 3)
					btn[i][j].setStyle("-fx-base: blue");
				panel.setPrefSize(200.0, 200.0);
				AnchorPane.setBottomAnchor(btn[i][j], 2.0);
				AnchorPane.setTopAnchor(btn[i][j], 1.0);
				AnchorPane.setLeftAnchor(btn[i][j], 1.0);
				AnchorPane.setRightAnchor(btn[i][j], 2.0);
				panel.getChildren().add(btn[i][j]);
				GridPane.setConstraints(panel, i, j);
				GameField.getChildren().add(panel);

			}
		}
	}

	public void onClickPane(ActionEvent event) {

	}

	@FXML
	void initialize() {
		assert GameField != null : "fx:id=\"GameField\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert statusLabel != null : "fx:id=\"statusLabel\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert explainLabel != null : "fx:id=\"explainLabel\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert nextButton != null : "fx:id=\"nextButton\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert scroll != null : "fx:id=\"scroll\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert message_display != null : "fx:id=\"message_display\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert ChatField != null : "fx:id=\"ChatField\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";
		assert sendButton != null : "fx:id=\"sendButton\" was not injected: check your FXML file 'GameMy_Screen4.fxml'.";

	}
}
