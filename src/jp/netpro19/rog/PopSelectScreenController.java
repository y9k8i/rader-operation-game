package jp.netpro19.rog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PopSelectScreenController implements Initializable{

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox messageDisp;

    @FXML
    private Label messageLable;

    @FXML
    private Button OkButton;

    @FXML
    private Button NoButton;

    @FXML
    void noOnClicked(ActionEvent event) {
    	if(messageLable.getText().equals("ここに接続しますか？")) {
    		Main.loader.<IpSelectScreenController> getController().setIsVisible(true);
    	}
    	Stage stage = (Stage)NoButton.getScene().getWindow();
    	stage.close();
    }

    @FXML
    void yesOnClicked(ActionEvent event) {
    	String fxmlString = Main.getFxmlName();
    	if(fxmlString.equals("load")) {
    		new Main().popViewLoad("しばらくお待ちください", "");
    		Main.gamePaneSetup();
    	}else if(!fxmlString.equals("")) {
    		new Main().changeView(fxmlString, "application.css");
    	}
    	if(messageLable.getText().equals("ここに接続しますか？")) {
    		Main.address = Main.loader.<IpSelectScreenController> getController().getAddress();
			RogData rog = new RogData("ping", Main.loader.<IpSelectScreenController> getController().getAddress());
			Main.sendRogData(rog);
		}
    	Stage stage = (Stage)OkButton.getScene().getWindow();
    	stage.close();
    }

    @Override
	public void initialize(URL location, ResourceBundle resources) {
		String message = Main.getText();
		messageLable.setText(message);
	}

    @FXML
    void initialize() {
        assert messageDisp != null : "fx:id=\"messageDisp\" was not injected: check your FXML file 'PopSelectScreen.fxml'.";
        assert messageLable != null : "fx:id=\"messageLable\" was not injected: check your FXML file 'PopSelectScreen.fxml'.";
        assert OkButton != null : "fx:id=\"OkButtob\" was not injected: check your FXML file 'PopSelectScreen.fxml'.";
        assert NoButton != null : "fx:id=\"NoButton\" was not injected: check your FXML file 'PopSelectScreen.fxml'.";

    }
}

