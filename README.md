# Rader Operation Game（レーダー作戦ゲーム）

<a href="../raw/develop/img/ゲーム画面.png"><img src="rader-operation-game/raw/develop/img/ゲーム画面.png" width="80%" /></a>

## GUIについて
フレームワークにはawtやSwingも使ったことあるが今回はJavaFXを採用
*  Java8になって機能が増えた
*  シーンビルダーで直感的にデザイン（FXMLとCSSを編集）出来る

構造としては(javafx.application.Application)→ウィンドウ→ビュー→部品 とする

## ネットワークについて
* リアルタイム性を重視するためUDPマルチキャスト通信を行い、コネクションの管理は行わない
* ポート番号及びグループアドレスはROGをエンコードしたU+52U+4FU+47より52447と52.4.47を使用
* クライアントとサーバの明確な区別はなく、**自ターンのインスタンスが**盤面をマルチキャストする

## データ型について
* 記載内容が増えたため[データ型一覧](データ型一覧.md)に記載

## ゲームシステムについて

フィールドの状態は専用クラス型の二次元配列にする<br>
<a href="../raw/develop/img/ゲーム画面とフロー.gif"><img src="rader-operation-game/raw/develop/img/ゲーム画面とフロー.gif" width="45%" /></a>
<a href="../raw/develop/img/画面デザイン.gif"><img src="rader-operation-game/raw/develop/img/画面デザイン.gif" width="45%" /></a>

## コードの管理について
* コーディングは任意のIDEで行う
* このGitLabで管理する
* [GitLab運用方針](GitLab運用方針.md)に従うこと
* 継続的インテグレーション機能を用いコミットすると自動でビルドが走るようにする
* メソッドの記述順序は[ここ](https://teratail.com/questions/139517 "メソッドの記述順序について")を参考にフィールド, イニシャライザー, メソッドをstaticなもの→publicなもの→privateなものの順にする
